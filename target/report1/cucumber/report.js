$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("SubmitTimesheet.feature");
formatter.feature({
  "line": 1,
  "name": "A user should be able to successfully submit a valid timesheet to a website via a form.",
  "description": "",
  "id": "a-user-should-be-able-to-successfully-submit-a-valid-timesheet-to-a-website-via-a-form.",
  "keyword": "Feature"
});
formatter.before({
  "duration": 1667490100,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "A user navigates to the rev2.force.com website",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "The user logs in with a valid username and password",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "The user clicks on the pencil icon in the timesheets section",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "The user fills in the hours they worked",
  "rows": [
    {
      "cells": [
        "8",
        "0"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The user enters some comments",
  "rows": [
    {
      "cells": [
        "\"A bot has submitted this timesheet You may reject it.\"",
        "\"This is an automation test. You may reject this timesheet\""
      ],
      "line": 10
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "The user clicks on the Submit button",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 27
    }
  ],
  "location": "SubmitTimesheetSteps.a_user_navigates_to_the_rev_force_com_website(int)"
});
formatter.result({
  "duration": 5200503900,
  "status": "passed"
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_logs_in_with_a_valid_username_and_password()"
});
formatter.result({
  "duration": 7295039700,
  "status": "passed"
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_clicks_on_the_pencil_icon_in_the_timesheets_section()"
});
formatter.result({
  "duration": 101548600,
  "status": "passed"
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_fills_in_the_hours_they_worked(DataTable)"
});
formatter.result({
  "duration": 118735800,
  "error_message": "org.openqa.selenium.ElementNotInteractableException: element not interactable\n  (Session info: chrome\u003d90.0.4430.212)\nBuild info: version: \u00273.6.0\u0027, revision: \u00276fbf3ec767\u0027, time: \u00272017-09-27T15:28:36.4Z\u0027\nSystem info: host: \u0027LIKEFUCKYOUGOKU\u0027, ip: \u0027192.168.56.1\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_281\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d90.0.4430.24 (4c6d850f087da467d926e8eddb76550aed655991-refs/branch-heads/4430@{#429}), userDataDir\u003dC:\\Users\\DOMINI~1\\AppData\\Local\\Temp\\scoped_dir19356_777789742}, timeouts\u003d{implicit\u003d0, pageLoad\u003d300000, script\u003d30000}, pageLoadStrategy\u003dnormal, unhandledPromptBehavior\u003ddismiss and notify, strictFileInteractability\u003dfalse, platform\u003dWINDOWS, proxy\u003dProxy(), goog:chromeOptions\u003d{debuggerAddress\u003dlocalhost:53394}, acceptInsecureCerts\u003dfalse, browserVersion\u003d90.0.4430.212, browserName\u003dchrome, javascriptEnabled\u003dtrue, platformName\u003dWINDOWS, setWindowRect\u003dtrue, webauthn:extension:largeBlob\u003dtrue, webauthn:virtualAuthenticators\u003dtrue}]\nSession ID: fce695eb967a58b71418829ef377490d\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:185)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:120)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:164)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:586)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:279)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.sendKeys(RemoteWebElement.java:100)\r\n\tat com.selenium.steps.SubmitTimesheetSteps.the_user_fills_in_the_hours_they_worked(SubmitTimesheetSteps.java:85)\r\n\tat ✽.And The user fills in the hours they worked(SubmitTimesheet.feature:7)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_enters_some_comments(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_clicks_on_the_Submit_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 13,
  "name": "The user submits valid data to the website",
  "description": "",
  "id": "a-user-should-be-able-to-successfully-submit-a-valid-timesheet-to-a-website-via-a-form.;the-user-submits-valid-data-to-the-website",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 14,
  "name": "The user should have their information be successfully submitted to the website.",
  "keyword": "Then "
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_should_have_their_information_be_successfully_submitted_to_the_website()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 4830079600,
  "status": "passed"
});
formatter.before({
  "duration": 1292649300,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "A user navigates to the rev2.force.com website",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "The user logs in with a valid username and password",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "The user clicks on the pencil icon in the timesheets section",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "The user fills in the hours they worked",
  "rows": [
    {
      "cells": [
        "8",
        "0"
      ],
      "line": 8
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "The user enters some comments",
  "rows": [
    {
      "cells": [
        "\"A bot has submitted this timesheet You may reject it.\"",
        "\"This is an automation test. You may reject this timesheet\""
      ],
      "line": 10
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "The user clicks on the Submit button",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 27
    }
  ],
  "location": "SubmitTimesheetSteps.a_user_navigates_to_the_rev_force_com_website(int)"
});
formatter.result({
  "duration": 5365723200,
  "status": "passed"
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_logs_in_with_a_valid_username_and_password()"
});
formatter.result({
  "duration": 7281174100,
  "status": "passed"
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_clicks_on_the_pencil_icon_in_the_timesheets_section()"
});
formatter.result({
  "duration": 100363200,
  "status": "passed"
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_fills_in_the_hours_they_worked(DataTable)"
});
formatter.result({
  "duration": 85026000,
  "error_message": "org.openqa.selenium.ElementNotInteractableException: element not interactable\n  (Session info: chrome\u003d90.0.4430.212)\nBuild info: version: \u00273.6.0\u0027, revision: \u00276fbf3ec767\u0027, time: \u00272017-09-27T15:28:36.4Z\u0027\nSystem info: host: \u0027LIKEFUCKYOUGOKU\u0027, ip: \u0027192.168.56.1\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_281\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d90.0.4430.24 (4c6d850f087da467d926e8eddb76550aed655991-refs/branch-heads/4430@{#429}), userDataDir\u003dC:\\Users\\DOMINI~1\\AppData\\Local\\Temp\\scoped_dir1112_374579525}, timeouts\u003d{implicit\u003d0, pageLoad\u003d300000, script\u003d30000}, pageLoadStrategy\u003dnormal, unhandledPromptBehavior\u003ddismiss and notify, strictFileInteractability\u003dfalse, platform\u003dWINDOWS, proxy\u003dProxy(), goog:chromeOptions\u003d{debuggerAddress\u003dlocalhost:53433}, acceptInsecureCerts\u003dfalse, browserVersion\u003d90.0.4430.212, browserName\u003dchrome, javascriptEnabled\u003dtrue, platformName\u003dWINDOWS, setWindowRect\u003dtrue, webauthn:extension:largeBlob\u003dtrue, webauthn:virtualAuthenticators\u003dtrue}]\nSession ID: 636011fa2539c7952b836f56ee5e76ad\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:185)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:120)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:164)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:586)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:279)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.sendKeys(RemoteWebElement.java:100)\r\n\tat com.selenium.steps.SubmitTimesheetSteps.the_user_fills_in_the_hours_they_worked(SubmitTimesheetSteps.java:85)\r\n\tat ✽.And The user fills in the hours they worked(SubmitTimesheet.feature:7)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_enters_some_comments(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_clicks_on_the_Submit_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 16,
  "name": "The user submits valid data, but their total hours for the week add up to an amount greater than 40",
  "description": "",
  "id": "a-user-should-be-able-to-successfully-submit-a-valid-timesheet-to-a-website-via-a-form.;the-user-submits-valid-data,-but-their-total-hours-for-the-week-add-up-to-an-amount-greater-than-40",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 17,
  "name": "The user should be notified that their hours are above 40",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "40",
      "offset": 55
    }
  ],
  "location": "SubmitTimesheetSteps.the_user_should_be_notified_that_their_hours_are_above(int)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 4828498000,
  "status": "passed"
});
});