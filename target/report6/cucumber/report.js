$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("SubmitTimesheet.feature");
formatter.feature({
  "line": 1,
  "name": "A user should be able to successfully submit a valid timesheet to a website via a form.",
  "description": "",
  "id": "a-user-should-be-able-to-successfully-submit-a-valid-timesheet-to-a-website-via-a-form.",
  "keyword": "Feature"
});
formatter.before({
  "duration": 15327318800,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "A user navigates to the rev2.force.com website",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "The user logs in with a valid username and password",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "The user clicks on the pencil icon in the timesheets section",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "The user fills in the hours they worked",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "The user enters some comments",
  "rows": [
    {
      "cells": [
        "\"A bot has submitted this timesheet You may reject it.\"",
        "\"This is an automation test. You may reject this timesheet\""
      ],
      "line": 9
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "The user clicks on the Submit button",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 27
    }
  ],
  "location": "SubmitTimesheetSteps.a_user_navigates_to_the_rev_force_com_website(int)"
});
formatter.result({
  "duration": 2113880900,
  "status": "passed"
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_logs_in_with_a_valid_username_and_password()"
});
formatter.result({
  "duration": 1160939200,
  "status": "passed"
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_clicks_on_the_pencil_icon_in_the_timesheets_section()"
});
formatter.result({
  "duration": 2543508400,
  "status": "passed"
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_fills_in_the_hours_they_worked()"
});
formatter.result({
  "duration": 18595600,
  "error_message": "java.lang.IndexOutOfBoundsException: Index: 3, Size: 1\r\n\tat java.util.ArrayList.rangeCheck(ArrayList.java:659)\r\n\tat java.util.ArrayList.get(ArrayList.java:435)\r\n\tat com.selenium.pages.TimesheetPage.fillHours(TimesheetPage.java:48)\r\n\tat com.selenium.steps.SubmitTimesheetSteps.the_user_fills_in_the_hours_they_worked(SubmitTimesheetSteps.java:51)\r\n\tat ✽.And The user fills in the hours they worked(SubmitTimesheet.feature:7)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_enters_some_comments(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_clicks_on_the_Submit_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 12,
  "name": "The user submits valid data to the website",
  "description": "",
  "id": "a-user-should-be-able-to-successfully-submit-a-valid-timesheet-to-a-website-via-a-form.;the-user-submits-valid-data-to-the-website",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "The user should have their information be successfully submitted to the website.",
  "keyword": "Then "
});
formatter.match({
  "location": "SubmitTimesheetSteps.the_user_should_have_their_information_be_successfully_submitted_to_the_website()"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 1750151800,
  "status": "passed"
});
});