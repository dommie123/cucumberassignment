Feature: A user should be able to successfully submit a valid timesheet to a website via a form. 

Background: 
	Given A user navigates to the rev2.force.com website
	When The user logs in with a valid username and password
	And The user clicks on the pencil icon in the timesheets section
	And The user fills in the hours they worked
	And The user enters some comments
	| "A bot has submitted this timesheet You may reject it." | "This is an automation test. You may reject this timesheet" |
	And The user clicks on the Submit button
	
Scenario: The user submits valid data to the website
	Then The user should have their information be successfully submitted to the website. 