package com.selenium.utils;

public class Constant {

	public static final String CHROME_DRIVER_DIRECTORY = System.getProperty("user.dir") + "\\src\\main\\resources\\drivers\\chromedriver.exe";
	public static final String GECKO_DRIVER_DIRECTORY = System.getProperty("user.dir") + "\\src\\main\\resources\\drivers\\geckodriver.exe";
	public static final String CONFIG_PROPERTIES_DIRECTORY = System.getProperty("user.dir") + "\\src\\main\\java\\com\\selenium\\properties\\config.properties";

}
