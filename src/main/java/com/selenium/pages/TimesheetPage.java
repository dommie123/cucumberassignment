package com.selenium.pages;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.selenium.steps.MasterHooks;

import cucumber.api.DataTable;

public class TimesheetPage extends BasePage {

	public @FindBy(xpath="//input[@placeholder='Username']") WebElement username;
	public @FindBy(xpath="//input[@placeholder='Password']") WebElement password;
	public @FindBy(xpath="//button[contains(text(),'Log in')]") WebElement loginButton;
	public @FindBy(xpath="//button[@title='Open Current Timesheet']") WebElement openTimesheetsButton;
	public @FindBy(xpath="//textarea[@name='requesterComments']") WebElement comments;
	public @FindBy(xpath="//body/div[@id='CustomerPortalTemplate']/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[2]/button[1]") WebElement submitTimesheetButton;
	
	public TimesheetPage() throws IOException {
		super();
	}
	
	public TimesheetPage getTimesheetPage() throws Exception {
		getDriver().get("https://rev2.force.com/revature/s/login/?ec=302&startURL=%2Frevature%2Fs%2F");
		return new TimesheetPage();
	}
	
	public TimesheetPage login() throws Exception {
		sendKeysToWebElement(username, "dominick.wiley@revature.net");
		sendKeysToWebElement(password, MasterHooks.password);
		waitAndClickElement(loginButton);
		return new TimesheetPage();
	}
	
	public TimesheetPage clickPencilButton() throws Exception {
		waitAndClickElement(openTimesheetsButton);
		return new TimesheetPage();
	}
	
	public TimesheetPage fillHours() throws Exception {
		Thread.sleep(2000);
		for (int i = 3; i < 7; i++) {
			sendKeysToWebElement(getDriver().findElements(By.xpath("//input[@step=1]")).get(i), "8.00");
		}
		return new TimesheetPage();
	}
	
	public TimesheetPage addComment(DataTable commentTable) throws Exception {
		List<List<String>> rawComments = commentTable.raw();
		sendKeysToWebElement(comments, rawComments.get(0).get(0));
		return new TimesheetPage();
	}

	public TimesheetPage submitTimesheet() throws Exception {
		waitAndClickElement(submitTimesheetButton);
		return new TimesheetPage();
	}
	
	public TimesheetPage confirmSubmissionSuccessful() throws Exception {
		Thread.sleep(1500);
		WebElement timesheetStatus = driver.findElement(By.xpath("//tbody/tr[1]/td[3]/div[1]"));
		assertEquals("https://rev2.force.com/revature/s/timesheet", driver.getCurrentUrl());
		assertEquals("submitted", timesheetStatus.getText().toLowerCase().replaceAll("[ ()0-9]", ""));
		return new TimesheetPage();
	}
}
