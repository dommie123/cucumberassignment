package com.selenium.steps;

import com.cucumber.listener.Reporter;
import com.selenium.utils.DriverFactory;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SubmitTimesheetSteps extends DriverFactory {
	
	@Given("^A user navigates to the rev(\\d+)\\.force\\.com website$")
	public void a_user_navigates_to_the_rev_force_com_website(int two) throws Throwable {
		System.out.println("Parameter " + two + " is pointless, but not the problem this time...");
		Reporter.addStepLog("Accessing Timesheets Page");
		timesheets.getTimesheetPage();
	}
	
	@When("^The user logs in with a valid username and password$")
	public void the_user_logs_in_with_a_valid_username_and_password() throws Throwable {
	    timesheets.login();
	}

	@And("^The user clicks on the pencil icon in the timesheets section$")
	public void the_user_clicks_on_the_pencil_icon_in_the_timesheets_section() throws Throwable {
		timesheets.clickPencilButton();
	}

	@And("^The user fills in the hours they worked$")
	public void the_user_fills_in_the_hours_they_worked() throws Throwable {
		timesheets.fillHours();
	}

	@And("^The user enters some comments$")
	public void the_user_enters_some_comments(DataTable comments) throws Throwable {
		timesheets.addComment(comments);
	}

	@And("^The user clicks on the Submit button$")
	public void the_user_clicks_on_the_Submit_button() throws Throwable {
		timesheets.submitTimesheet();
	}

	@Then("^The user should have their information be successfully submitted to the website\\.$")
	public void the_user_should_have_their_information_be_successfully_submitted_to_the_website() throws Throwable {
		timesheets.confirmSubmissionSuccessful();
	}
}
