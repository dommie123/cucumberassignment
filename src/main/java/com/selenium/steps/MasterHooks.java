package com.selenium.steps;

import java.util.Scanner;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.selenium.pages.BasePage;
import com.selenium.utils.DriverFactory;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class MasterHooks extends DriverFactory {
	public static String password;
	
	@Before
	public void setUp() {
		driver = getDriver();
		driver.manage().window().maximize();
		
		Scanner s = new Scanner(System.in);
		System.out.println("Please enter the password that will be used for the test (default is the value in REV2_PASSWORD environment variable): ");
		password = s.nextLine();
		if (password.isEmpty()) 
			password = System.getenv("REV2_PASSWORD");
		s.close();
		
	}
	
	@After
	public void tearDownAndScreenshotOnFailure(Scenario scenario) {
		try {
			if (driver != null) {
				if (scenario.isFailed()) {
					scenario.embed(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES), "image/png");
					BasePage.captureScreenshot();
				}
				driver.manage().deleteAllCookies();
				driver.close();
				driver.quit();
				driver = null;
			}
		} catch (Exception e) {
			System.out.println("Method failure, tearDownAndScreenshotOnFailure! Exception: " + e.getMessage());
		}
	}
}
